module.exports = function(server){
	var config = require('../config/config');
	// var auth = require('../auths/auth');

	server.get('/', function(req, res, next){
		res.status(200);
		return res.json(config.responseMessage(200, true, 'Welcome to PUSPApps REST API Services', ''));
	});
	
	/* v1.0.0 */
	var Kabayanv100 = require('../controllers/v1.0.0/kabayan.controller.js');

	server.post({path: '/kabayan/sentimentscoring', version: '1.0.0'}, Kabayanv100.sentimentScoring);
	server.post({path: '/kabayan/wordscounter', version: '1.0.0'}, Kabayanv100.wordsCounter);
	/* v2.0.0 */
	
}
