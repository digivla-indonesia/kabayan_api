var path = require('path');
var async = require('async');
var mysql = require('mysql');
var config = require('../../config/config');
var Forum = require('../../models/forum.model');
var User = require('../../models/user.model');
var TbSentimentWords = require('../../models/tb_sentiment_words.model');


var pool  = mysql.createPool({
	connectionLimit : 10,
	host            : config.mysqlDBHost,
	user            : config.alMysqlUsername,
	password        : config.alMysqlPassword,
	database        : config.mysqlDBWordCloud
});


function kabayanController(){
	this.wordsCounter = function(req, res, next){
		var content = req.body.content || '',
			keyword = req.body.keyword || '',
			datee = req.body.date || '',
			client_id = req.body.client_id || '';

		if(content === '' || keyword === '' || datee === '' || client_id === ''){
			res.status(500);
			return res.json(config.responseMessage(500, false, 'Failed', 'You have to fill all parameters (content, keyword, article_id, datee, client_id)'));
		}

		async.waterfall([
			function(cb){
				checkClientTable(client_id, function(err, results){
					if(err){
						return cb(err, null);
					}

					cb(null, results);
				});
			},
			function(tableClient, cb){
				var resArr = [];
				var x = content.split(/[^a-zA-Z\u00C0-\u017F]+/).reduce(function (a, b) {
					if (b.length > 2) a[b] ? a[b]++ : a[b] = 1;

					return a;
				}, {});


				var excludeWords = ["di", "ke", "dari", "ya", "pada", "mengapa", "kenapa", "dimana", "sesudah", "sementara", "sebelum", "ketika", "sehabis", "setelah", "sehingga", "sejak", "selesai", "tatkala", "sambil", "seraya", "selagi", "selama", "sampai", "jika", "jikalau", "kalau", "asal", "bila", "asalkan", "manakala", "andaikan", "seandainya", "sekiranya", "seumpamanya", "agar", "supaya", "biar", "biarpun", "meskipun", "walaupun", "walau", "sunguhpun", "kendatipun", "seakan", "seakan-akan", "sebagaimana", "seolah-olah", "seolah", "seperti", "sebagai", "bagaikan", "laksana", "sebab", "oleh karena", "oleh", "karena", "sehingga", "sampai-sampai", "maka", "makanya", "karenanya", "bahwa", "dengan", "melalui", "begitu", "sekalipun", "itu", "selanjutnya", "pula", "bahwasanya", "sesungguhnya", "malah", "malahan", "bahkan", "akan", "tetapi", "namun", "kecuali", "serta", "lagi", "pula", "lagipula", "melainkan", "sedangkan", "padahal", "sebaliknya", "entah", "maupun", "atau", "apabila", "bilamana", "sedari", "seraya", "semenjak", "guna", "untuk", "akibatnya", "sampe", "bagai", "ibarat", "umpama", "semakin", "sedemikian", "rupa", "kian", "sungguhpun", "mula-mula", "lalu", "kemudian", "selain", "terutama", "yang", "dan", "-", "ini", ",",".", "\"", "\'", "\r", "\n", "http", "https"];

				for(var attributename in x){
				    if(excludeWords.indexOf(attributename.toLowerCase()) < 0){
				    	resArr.push([attributename.toLowerCase(), x[attributename]]);
				    }
				}

				resArr = resArr.sort(function(a,b){
				    return a[1] - b[1];
				});

				cb(null, resArr);
			},
			function(words, cb){
				var insertQuery = 'INSERT INTO ' + client_id + ' (word, value, datee) VALUES ';

				for(var i=0;i<words.length;i++){
					if(i + 1 == words.length){
						insertQuery += '("' + words[i][0] + '", '+ words[i][1] +', "' + datee + '") ';
					} else{
						insertQuery += '("' + words[i][0] + '", '+ words[i][1] +', "' + datee + '"), ';
					}
				}

				insertQuery += 'ON DUPLICATE KEY UPDATE value = value + VALUES(value)';

				pool.getConnection(function(err, connection) {
					if (err) throw err;
					// Use the connection
					connection.query(insertQuery, function (error, results, fields) {
						connection.release();

						if(error){
							return cb(error, null);
						}

						return cb(null, words);
					});
				});
			}
		], function(err, result){
			if(err){
				res.status(500);
				return res.json(config.responseMessage(500, false, 'Failed', err));
			}

			res.status(200);
			return res.json(config.responseMessage(200, true, 'Success', 'Success insert to ' + client_id));
		});
	};

	this.sentimentScoring = function(req, res, next){
		var content = req.body.content,
			keyword = req.body.keyword,
			article_id = req.body.article_id;


		async.waterfall([
			function(cb){

				var my_content = "";
				var mentiontimes = 0;
				var sentiment = 0;

				if(content){
					var re = new RegExp(keyword.trim().toLowerCase(), "ig");
					mentiontimes = content.toLowerCase().match(re);

					console.log('mentiontimes: ' + mentiontimes);
					cb(null, mentiontimes);
				}

			},
			function(mentions, cb){

				var split_words = content.split(/\s/m);
				var cr = [];
				split_words.forEach(function(subject, i){
					subject = subject.replace(/([A-Za-z])\1{2,}/m, '\1');
					subject = subject.replace(/[^A-Za-z ]/m, '');
					

					search_word(subject, function(words){
						if(typeof words === 'object' && words.length > 0){
							cr.push({
								value: parseInt(words[0].value),
								type: parseInt(words[0].type)
							});
						} else{
							cr.push({
								value: 0,
								type: -1
							});
						}

						if(split_words.length - 1 === i){
							cb(null, split_words, mentions, cr);
						}
					});
				});


			},
			function(split_words, mentions, cr, cb){


				var sen = 0,
					flag = 0,
					fn = 0,
					Arrsen = [],
					x, an, n;

				for(var i=0;i<cr.length;i++){

					if(cr[i].type == 2){
						if(flag == 0){
							flag = 1;
						} else{
							flag = 0;
						}
					}

					if(cr[i].type == 3){
						an = cr[i].value;
					}

					if(cr[i].type == 4){
						n = Arrsen.length;
						Arrsen[n - 1] *= cr[i].value;
					}

					if (flag == 0 && cr[i].type < 3) {
						Arrsen.push(cr[i].value);
					} else if(Math.abs(cr[i].type) == 1) {
						Arrsen.push(fn * cr[i].value);
						if (typeof an !== 'undefined' && an > 0) {
							Arrsen[Arrsen.length - 1] *= an;
							an = 0;
						}
					}
				}

				for(var i=0; i<Arrsen.length; i++) {
				    sen += Arrsen[i];
				}

				for(var i=0;i<cr.length;i++){
					cr[i].word = split_words[i];
				}

				if (sen < -5){
					console.log('sen < -5');
					sen = -2;
				} else if(sen < 0){
					console.log('sen < 0');
					sen = -1;
				} else if (sen > 5){
					console.log('sen > 5');
					sen = 2;
				} else if (sen > 0){
					console.log('sen > 0');
					sen = 1;
				} else{
					console.log('else');
					sen = 0;
				}
				
				cb(null, mentions, sen);


			},
			function(mentions, sentiment, cb){
				var tone_level = 3;
			 	var sen = 0;
			 	if (tone_level === 3) {
			    	var if_lev = 1;
					if (sentiment === -2){
			        	sen = -1;
			        	if_lev = 2;
					} else if (sentiment === 2){
						sen = 1;
			        	if_lev = 3;
					}
				}

				cb(null, {sentiment: sen, article_id: article_id});
			}
		], function(err, result){
			if(err){
				res.status(500);
				return res.json(config.responseMessage(500, false, 'Failed', {}));
			}

			res.status(200);
			return res.json(config.responseMessage(200, true, 'Success', {article_id: article_id, sentiment: result}));
		});
	};

	return this;
}


/* Helper */
function check_tone_degree(sentiment){
 	var tone_level = 3;
 	var sen = 0;
console.log('sentiment: ' + sentiment + ', typeof: ' + typeof sen);
 	if (tone_level === 3) {
    	var if_lev = 1;
		if (sentiment === -2){
        	sen = -1;
        	if_lev = 2;
		} else if (sentiment === 2){
			sen = 1;
        	if_lev = 3;
		}
	}

	return sen;
}

function getMentiontime(content, keyword){
	var my_content = "";
	var mentiontimes = 0;
	var sentiment = 0;

	if(content){
		var re = new RegExp(keyword, "ig");
		mentiontimes = content.toLowerCase().match(re);

		return {
			mentiontimes: mentiontimes,
			sentiment: getSentiment(content)
		};
	}
}

function getSentiment(subject){
	var split_words = subject.split(/\s/m);
	count_sentiment(split_words, function(cr){

		var sen = 0,
			flag = 0,
			fn = 0,
			Arrsen = [],
			x, an, n;

		for(var i=0;i<cr.length;i++){
			if(cr[i].type == 2){
				if(flag == 0){
					flag = 1;
				} else{
					flag = 0;
				}
			}

			if(cr[i].type == 3){
				an = cr[i].value;
			}

			if(cr[i].type == 4){
				n = Arrsen.length;
				Arrsen[n - 1] *= cr[i].value;
			}

			if (flag == 0 && cr[i].type < 3) {
				Arrsen.push(cr[i].value);
			} else if(Math.abs(cr[i].type) == 1) {
				Arrsen.push(fn * cr[i].value);
				if (typeof an !== 'undefined' && an > 0) {
					Arrsen[Arrsen.length - 1] *= an;
					an = 0;
				}
			}
		}

		for(var i=0; i<Arrsen.length; i++) {
		    sen += Arrsen[i];
		}

		for(var i=0;i<cr.length;i++){
			cr[i].word = split_words[i];
		}

		if (sen < -5){
			console.log('sen < -5');
			sen = -2;
		} else if(sen < 0){
			console.log('sen < 0');
			sen = -1;
		} else if (sen > 5){
			console.log('sen > 5');
			sen = 2;
		} else if (sen > 0){
			console.log('sen > 0');
			sen = 1;
		} else{
			console.log('else');
			sen = 0;
		}
		
		return sen;
	});
}

function count_sentiment(split_words, cb) {
	var cr = [];
	split_words.forEach(function(subject, i){
		subject = subject.replace(/([A-Za-z])\1{2,}/m, '\1');
		subject = subject.replace(/[^A-Za-z ]/m, '');
		

		search_word(subject, function(words){
			if(typeof words === 'object' && words.length > 0){
				// cb({
				// 	value: words[0].value,
				// 	type: words[0].type
				// });
				cr.push({
					value: parseInt(words[0].value),
					type: parseInt(words[0].type)
				});
			} else{
				// cb({
				// 	value: 0,
				// 	type: -1
				// });
				cr.push({
					value: 0,
					type: -1
				});
			}

			if(split_words.length - 1 === i){
				cb(cr);
			}
		});
	});
}

function search_word(word, cb) { 
	TbSentimentWords.findAll({
		attributes: ['value', 'type'],
		where: {'word': word}
	}).then(function(words){
		cb(words);
	});
}


function checkClientTable(client_id, callback){
	pool.getConnection(function(err, connection) {
		if (err) throw err;
		var options = {sql: "DESCRIBE " + client_id};
		// Use the connection
		connection.query(options.sql, function (error, results, fields) {
			// console.log('error: ' + JSON.stringify(error));
			// console.log('results: ' + JSON.stringify(results));
			// console.log('fields: ' + JSON.stringify(fields));

			if(error){
				options = {sql: 'CREATE TABLE `' + client_id + '` (`word` varchar(255) NOT NULL, `value` int(32) NOT NULL, `datee` date NOT NULL, PRIMARY KEY (`word`,`datee`), KEY `word` (`word`)) ENGINE=MyISAM DEFAULT CHARSET=latin1' };
				connection.query(options.sql, function (error, results, fields) {
					// And done with the connection.
					connection.release();

					if (error) return callback(error, null);
					return callback(null, results);
				});
			} else{
				// And done with the connection.
				connection.release();

				if (error) return callback(error, null);
				return callback(null, results);
			}

		});
	});
}


module.exports = new kabayanController();
