var restify = require('restify');
var versioning = require('restify-url-semver');
var mongoose = require('mongoose');
var jwt    = require('jsonwebtoken');
var config = require('./config/config');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var server = restify.createServer({name: 'Naung REST API'});

server.pre(restify.pre.sanitizePath());
server.pre(versioning({ prefix: '/api' }));
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(function crossOrigin(req, res, next){
	/**
     * Response settings
     * @type {Object}
     */
    var responseSettings = {
        "AccessControlAllowOrigin": req.headers.origin,
        "AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
        "AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
        "AccessControlAllowCredentials": false
    };

    /**
     * Headers
     */
    res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
    res.header("Access-Control-Allow-Origin",  responseSettings.AccessControlAllowOrigin);
    res.header("Access-Control-Allow-Headers", (req.headers['access-control-request-headers']) ? req.headers['access-control-request-headers'] : "x-requested-with");
    res.header("Access-Control-Allow-Methods", (req.headers['access-control-request-method']) ? req.headers['access-control-request-method'] : responseSettings.AccessControlAllowMethods);

    if ('OPTIONS' == req.method) {
        res.send(200);
    }
    else {
        next();
    }
});
server.use(function(req, res, next){
	var path = req.path();
	
	/* DEVELOPMENT NEED*/
	if(path === '/testuser') return next();
	if(path === '/testgetforum') return next();
	if(path === '/testlogin') return next();
	/* DEVELOPMENT NEED*/

	if(req.query.ak !== config.password){
		res.status(401)
		return res.json(config.responseMessage(401, false, 'Access Key Required', ''));
	}
	
	// No authentication needed
	// if(path === '//client/login' || path === '//client/register' || path === '//client/forgetpassword') return next();
	
	// Token validating
	// var tokens = decodeURIComponent(req.headers.authorization);
	// if(tokens){
	// 	jwt.verify(tokens, config.jwtsecret, function(err, decoded){
	// 		if(err){
	// 			res.status(200);
	// 			return res.json(config.responseMessage(401, false, err.name, 'You have to login'));
	// 		}
			
	// 		next();
	// 	});
	// } else{
	// 	res.status(200);
	// 	return res.json(config.responseMessage(401, false, 'There is no token!!', ''));
	// }







	if (req.headers.authorization) {
      auth = new Buffer(req.headers.authorization.substring(6), 'base64').toString().split(':');
    }

    if (!auth || auth[0] !== 'kabayan' || auth[1] !== 'w[2VHaQt8)_ZQn7+') {
		// res.statusCode = 401;
		// res.setHeader('WWW-Authenticate', 'Basic realm="MyRealmName"');
		// res.json({"STATUS": "FAILED", "DATA" : "Unauthorized", "TIME": fomratted.toString()});
		res.status(401);
		return res.json(config.responseMessage(401, false, err.name, 'You are not authorized!'));

    }else{
		// var _send = res.send;
		// var sent = false;
		// res.send = function(data){
		// 	if(sent) return;
		// 	_send.bind(res)(data);
		// 	sent = true;
		// };
		next();
	}
	
	// next();
});

server.on('uncaughtException', function (req, res, err, cb) {
    console.log(err);
    return cb();
});

server.listen(config.port, function() {
	console.log('%s is listening at %s', server.name, '3031');
});

var routes = require('./routes/routes')(server);
