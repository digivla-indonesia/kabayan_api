var Sequelize = require("sequelize");
var config = require('../config/config');

var sequelize = new Sequelize(config.mysqlDBMedia, config.mysqlDBUsername, config.mysqlDBPassword, {
	host: config.mysqlDBHost,
	port: config.mysqlDBPort,
	dialect: 'mysql'
});

var NewsMedia = sequelize.define('NewsMedia', {
	media_id: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	media_name: Sequelize.STRING,
	media_type_id: Sequelize.INTEGER,
	circulation: Sequelize.INTEGER,
	rate_bw: Sequelize.INTEGER,
	rate_fc: Sequelize.INTEGER,
	language: Sequelize.STRING,
	statuse: Sequelize.STRING,
	usere: Sequelize.STRING,
	pc_name: Sequelize.STRING,
	input_date: Sequelize.DATE
}, {
	tableName: 'tb_media',
	timestamps: false
});

module.exports = NewsMedia;