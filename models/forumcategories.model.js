module.exports = (function forumCategories () {
 
	var mongoose = require('../config/db').mongoose;
 
	var schema = {
		name: {type: String},
		thread: [{
			type: mongoose.Schema.Types.ObjectId,
	 		ref: 'forums'
		}]
	};
	
	var collectionName = 'forumcategories';
	var forumCategoriesSchema = mongoose.Schema(schema, { timestamps: true });
	var forumCategory = mongoose.model(collectionName, forumCategoriesSchema);
	
	return forumCategory;
})();