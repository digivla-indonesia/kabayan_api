var Sequelize = require("sequelize");
var config = require('../config/config');

var sequelize = new Sequelize(config.mysqlDBArticle, config.mysqlDBUsername, config.mysqlDBPassword, {
	host: config.mysqlDBHost,
	port: config.mysqlDBPort,
	dialect: 'mysql'
});

var NewsArticle = sequelize.define('NewsArticle', {
	article_id: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	title: Sequelize.STRING,
	media_id: Sequelize.INTEGER,
	datee: Sequelize.STRING,
	content: Sequelize.TEXT,
	mmcol: Sequelize.INTEGER,
	circulation: Sequelize.INTEGER,
	page: Sequelize.STRING,
	file_pdf: Sequelize.STRING,
	columne: Sequelize.INTEGER,
	size_jpeg: Sequelize.INTEGER,
	journalist: Sequelize.STRING,
	rate_bw: Sequelize.INTEGER,
	rate_fc: Sequelize.INTEGER,
	is_chart: Sequelize.INTEGER,
	is_table: Sequelize.INTEGER,
	is_colour: Sequelize.INTEGER,
}, {
	tableName: 'tb_articles',
	timestamps: false
});

module.exports = NewsArticle;