module.exports = (function infoModel () {
 
	var mongoose = require('../config/db').mongoose;
 
	var schema = {
		started: {
			creator: {type: String},
			timestamp: {type: Date, "default": Date.now()},
			image: {type: String, "default": ""},
			content: {type: String}
		},
		title: {type: String},
		active: {type: Boolean, "default": true},
		category: {
			id: {type: String},
			name: {type: String}
		},
		viewed: {type: Number, "default": 1}
	};
	
	var collectionName = 'infos';
	var infoSchema = mongoose.Schema(schema, { timestamps: true });
	var info = mongoose.model(collectionName, infoSchema);
	
	return info;
})();