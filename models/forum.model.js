module.exports = (function forumModel () {
 
	var mongoose = require('../config/db').mongoose;
 
	var schema = {
		started: {
			client: {
				type: mongoose.Schema.Types.ObjectId,
		 		ref: 'clients'
			},
			timestamp: {
				type: Date, 
				default: Date.now()
			},
			content: {
				type: String
			}
		},
		title: {
			type: String
		},
		comments: [{
			_id : {
				type: Number,
				default: Date.now()
			},
            client: {
            	type: mongoose.Schema.Types.ObjectId,
		 		ref: 'clients'
            },
            content : {
            	type: String,
            	default: ""
            },
            timestamp : {
            	type: Date,
            	default: new Date().toString()
            },
            active : {
            	type: Boolean,
            	default: true
            }
		}],
		active: {
			type: Boolean,
			default: true
		},
		category: {
			type: mongoose.Schema.Types.ObjectId,
		 	ref: 'forumcategories'
		},
		viewed: {
			type: Number, 
			default: 1
		}
	};
	
	var collectionName = 'forums';
	var forumSchema = mongoose.Schema(schema, { timestamps: true });
	var forum = mongoose.model(collectionName, forumSchema);
	
	return forum;
})();