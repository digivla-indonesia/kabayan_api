module.exports = (function forumCategories () {
 
	var mongoose = require('../config/db').mongoose;
 
	var schema = {
		name: {type: String},
		thread: {type: Array, "default": []},
	};
	
	var collectionName = 'infocategories';
	var forumCategoriesSchema = mongoose.Schema(schema, { timestamps: true });
	var forumCategory = mongoose.model(collectionName, forumCategoriesSchema);
	
	return forumCategory;
})();
