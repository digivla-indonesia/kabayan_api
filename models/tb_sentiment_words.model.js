var Sequelize = require("sequelize");
var config = require('../config/config');

var sequelize = new Sequelize(config.mysqlDBTemp, config.mysqlDBUsername, config.mysqlDBPassword, {
	host: config.mysqlDBHost,
	port: config.mysqlDBPort,
	dialect: 'mysql'
});

var TbSentimentWords = sequelize.define('tb_sentiment_words', {
	word: {
		type: Sequelize.STRING,
		primaryKey: true
	},
	lang: Sequelize.STRING,
	value: Sequelize.INTEGER,
	type: Sequelize.INTEGER
}, {
	tableName: 'tb_sentiment_words',
	timestamps: false
});

module.exports = TbSentimentWords;