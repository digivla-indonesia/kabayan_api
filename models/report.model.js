module.exports = (function reportModel () {
 
	var mongoose = require('../config/db').mongoose;
 
	var schema = {
		creator: {
			client: {
				type: mongoose.Schema.Types.ObjectId, 
				ref: 'clients'
			},
			phone: {
				type: String,
				default: ''
			},
		},
		location: {
			longitude: {
				type: Number
			},
			latitude: {
				type: Number
			}
		},
		active: {
			type: Boolean,
			default: true
		},
		processed: {
			type: Boolean,
			default: false
		},
		started: {
			content: {
				type: String
			},
			timestamp: {
				type: Date,
				default: Date.now()
			}
		},
		posts: [{
			_id : {
				type: Number, 
				default: Date.now()
			},
   		 	/*client : {
	 			type: mongoose.Schema.Types.ObjectId,
	 			ref: 'clients'
	 		},
	 		role: {
    	    	type: String, 
    	    	default: 'clients'
    	    },*/
	 		user: {
	 			_id: {
	 				type: String,
	 				default: ''
	 			},
	 			name: {
	 				type: String,
	 				default: ''
	 			},
	 			role: {
	 				type: String,
	 				default: 'client'
	 			}
	 		},
    	    content : {
    	    	type: String, 
    	    	default: ""
    	    },
    	    timestamp : {
    	    	type: Date, 
    	    	default: new Date().toString()
    	    },
    	    active : {
    	    	type: Boolean, 
    	    	default: true
    	    }
		}],
		category: {
			type: String,
			required: 'Harus memiliki kategori'
		}
	};
	
	var collectionName = 'reports';
	var reportSchema = mongoose.Schema(schema, { timestamps: true });
	var report = mongoose.model(collectionName, reportSchema);
	
	return report;
})();