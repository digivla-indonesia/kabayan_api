module.exports = (function users () {
 
	var mongoose = require('../config/db').mongoose;
 
	var schema = {
		email: {
			type: String, 
			index: true, 
			match: /.+\@.+\..+/
		},
		name: {
			type: String
		},
		password: {
			type: String
		},
		activation: {
			status: {
				type: Boolean, 
				default: false
			},
			token: {
				type: String
			}
		},
		active: {
			type: Boolean,
			default: true
		},
		forums: {
			owning: [{
				type: mongoose.Schema.Types.ObjectId,
	 			ref: 'forums'
			}],
			posts: [{
				forumid: {
					type: mongoose.Schema.Types.ObjectId,
		 			ref: 'forums'
				},
				postid: {
					type: Number,
					default: Date.now()
				},
				timestamp: {
					type: Date,
					default: new Date().toString()
				}
			}]
		},
		reports: [{
				type: mongoose.Schema.Types.ObjectId,
	 			ref: 'reports'
		}],
		/*Additional info*/
		avatar: {
			type: String,
			default: ''
		},
		phone: {
			type: String,
			default: ''
		},
		regid: {
			type: String,
			default: ''
		}
		/*Additional info*/
	};
	
	var collectionName = 'clients';
	var userSchema = mongoose.Schema(schema, { timestamps: true });
	var user = mongoose.model(collectionName, userSchema);
	
	userSchema.pre('save', function(next){
		var self = this;
		user.count({email: self.email}, function(err, c){
			if(c==0){
				next();
			} else{
				console.log('user exists: ' + self.email);
				next(new Error("User already exists!"));
				//next();
			}
		});
	});
	
	return user;
})();