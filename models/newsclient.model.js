var Sequelize = require("sequelize");
var config = require('../config/config');

//var sequelize = new Sequelize('zvamu', 'harry', 'c1nt4h4n1f', {
var sequelize = new Sequelize(config.mysqlDBClient, config.mysqlDBUsername, config.mysqlDBPassword, {
	host: config.mysqlDBHost,
	port: config.mysqlDBPort,
	dialect: 'mysql'
});

var NewsClient = sequelize.define('NewsClient', {
	article_id: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	category_id: {
		type: Sequelize.STRING,
		primaryKey: true
	},
	//datee: Sequelize.DATE,
	datee: Sequelize.STRING,
	media_id: Sequelize.INTEGER,
	mention_times: Sequelize.INTEGER,
	tone: Sequelize.INTEGER,
	exposure: Sequelize.INTEGER,
	advalue_fc: Sequelize.INTEGER,
	circulation: Sequelize.INTEGER,
	advalue_bw: Sequelize.INTEGER,
	data_input_date: Sequelize.DATE,
	usere: Sequelize.STRING,
	pc_name: Sequelize.STRING
}, {
	tableName: 'zvamu',
	timestamps: false
});

module.exports = NewsClient;