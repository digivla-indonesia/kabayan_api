var jwt = require('jsonwebtoken');
var User = require('../models/user.model.js');
var config = require('../config/config');
var bcrypt = require('bcrypt');

var auth = {
	login: function(req, res) {

		var email = req.params.email || '';
		var password = req.params.password || '';

		if (email == '' || password == '') {
			res.status(200);
			return res.json(config.responseMessage(200, false, 'Invalid credentials', ''));
		}

		// Fire a query to your DB and get the user object if it exists
		auth.getUser(email, function(dbUserObj,err) {
			if (!dbUserObj) { // If authentication fails, we send a 401 back
				res.status(200);
				return res.json(config.responseMessage(200, false, 'Invalid credentials', ''));
			}

			if (dbUserObj) {
				// If authentication is success, we will generate a token
				// and dispatch it to the client
				bcrypt.compare(password,dbUserObj.password, function(err, passmatch) {
					if (passmatch == true) {
						/**
						  *		GENERATING TOKEN HERE
						  */
						
						if(!dbUserObj.active){
							res.status(200);
							return res.json(config.responseMessage(200, false, 'User is inactive', ''));
						}
						
						// var profile = {
						// 	email: dbUserObj.email,
						// 	name: dbUserObj.name,
						// 	id: dbUserObj._id
						// };

						var token = jwt.sign(profile, config.jwtsecret, {
							//expiresIn: "7d"
							expiresIn: "1y"
						});

						res.status(200);
						return res.json(config.responseMessage(200, true, 'Success', {
							"success": true,
							"token": token
							// "userid": dbUserObj._id,
							// "username": dbUserObj.name,
							// "email": dbUserObj.email,
							// "phone": dbUserObj.phone
						}));
					} else {
						res.status(200);
						return res.json(config.responseMessage(200, false, 'Invalid credentials', ''));
					}
				});
			}
		});
	},

	getUser: function(email, callback) {
		//User.findOne({ username: username }, function (err,user) {
		User.findOne({ email: email }, function (err,user) {
			if (err) {
				console.log(err);
				callback(false);
			} else {
				/*user.regid = regid;
				
				user.save(function(err){
					if (err) {
						console.log(err);
						callback(false);
					}
					
					callback(user);
				});*/
				callback(user);
			}
		});
		/*User.findOneAndUpdate({
			email: email
		}, {
			$set: {
				regid: regid
			}
		}, function(err, clients){
			if(err){
        		console.log(err);
				callback(false);
    		}

			callback(clients);
		});*/
	},

	isUserAdmin: function(username,callback) {
		User.findOne({ username: username }, function (err,user) {
			if (user.role == 'admin') {
				callback(true);
			} else {
				callback(false);
			}
		});
	},

	encryptPass: function(password,callback) {
		// this will auto-gen a salt
		// bcrypt.hash(password,size of hash, function)
		bcrypt.hash(password, 12, function(err, hash) {
			if (hash) {
				callback(hash);
			} else {
				console.log(err);
			}
		});
	}
};

/** private methods **/

// generate token
function genToken(user) {
	var expires = expiresIn(7); // 7 days
	var token = jwt.encode({
		exp: expires
	}, config.jwtsecret);

	return {
		token: token,
		expires: expires,
		user: user.username
	};
}

function expiresIn(numDays) {
	var dateObj = new Date();
	return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;
